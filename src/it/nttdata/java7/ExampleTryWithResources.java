package it.nttdata.java7;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ExampleTryWithResources {
	public static void main(String[] args) throws IOException, SQLException {
		FileWriter filew = null;
		try {
			filew = new FileWriter("filewname");
			filew.flush();
		} catch (IOException e) {
			try {
				filew.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}

		// Si può scrivere in parentesi tonde del try
		// il codice che lancia l'eccezione se il tipo di classe implementa
		// closeable
		try (FileWriter fw = new FileWriter("fw")) {
			fw.write("bye bye");
			fw.flush();
		}
		String url = "someting";
		try (Connection conn = DriverManager.getConnection(url)) {

		}
	}

	private static void manageResource(String url) {
		try (Connection conn = DriverManager.getConnection(url)) {

		} catch (SQLException sqle) {
			sqle.getSuppressed();
		}
	}
}
