package it.nttdata.script;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptExample {
	public static void main(String[] args) {
		ScriptEngineManager manager = new ScriptEngineManager();

		// Per usare il motore di java 7
		// ScriptEngine engine = manager.getEngineByName("javascript");
		ScriptEngine engine = manager.getEngineByName("nashorn");
		try {
			// engine.eval("print('hello!')");

			try (FileReader fr = new FileReader("Script.js")) {
				engine.eval(fr);
				Invocable inv = (Invocable) engine;
				inv.invokeFunction("hello", "Giovanna");

				// avendo definito una function run dentro Script.js
				// inv può subire il cast a Runnable, tramite il metodo
				// getInterface(...)
				Runnable casted = inv.getInterface(Runnable.class);

				// la stessa cosa si può fare su una variabile globale presente
				// nella js
				Object obj = engine.get("obj");
				inv.invokeMethod(obj, "run", null);

				Runnable castedObj = inv.getInterface(obj, Runnable.class);
				castedObj.run();

				// Creiamo un oggetto java e passiamolo al .js tramite un put
				Runnable jo = () -> System.out
						.println("Hello from java object");

				engine.put("jo", jo);
				inv.invokeFunction("testJavaObj");

				// dopo aver definito proprio un oggetto java (javaType...) nel
				// .js
				// posso invocarlo come un vero e proprio java object
				Runnable jsObj = (Runnable) engine.get("jsObj");
				jsObj.run();

				inv.invokeFunction("testCambioRun");

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ScriptException e) {
			e.printStackTrace();
		}

	}
}
