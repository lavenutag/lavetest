package it.nttdata.nio.file;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class NioFileExample {
	public static void aMethod() throws IOException {

		File file = new File("TestJava8.txt");
		Path path = file.toPath();
		try (Stream<String> lines = Files.lines(path)) {
			lines.count();
		} catch (UncheckedIOException uioe) {

		}
	}
}
