package it.nttdata.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleMap {
	public static void main(String[] args) {
		Map<String, Item> map = new HashMap<>();
		String key = "element 1";
		exampleWithLambda(map, key);

	}

	private static void example(Map<String, Item> map, String key) {
		Item x = map.get(key);
		if (x == null) {
			x = new Item();
			map.put(key, x);
		}
	}

	private static void exampleWithLambda(Map<String, Item> map, String key) {
		Item x = map.computeIfAbsent(key, (k) -> {
			Item i = new Item();
			return i;
		});
		System.out.println(x.toString());
	}

	private static void exampleWithLambdaList(Map<String, List<Item>> map,
			String key, Item x) {
		String dep = x.getDepartment();

		List<Item> list = map.computeIfAbsent(dep, (k) -> {
			return new ArrayList<>();
		});
		list.add(x);

	}

	private static void exampleForeach(List<String> list) {
		for (String s : list) {
			// do somethig
		}

		// due modi diversi per scrivere la stessa cosa.

		list.forEach(s -> {
			// do something
		});
	}

	private static void exampleCompute() {
		Map<String, Item> map = new HashMap<>();
		String key = "";
		map.compute(key, (k, oldV) -> {

			return oldV != null ? oldV : new Item();

		});
	}

	private static void exampleRemoveIf(List<Item> list) {
		list.removeIf(e -> e == null);
	}

	private static void exampleReplaceAll(List<Item> list) {
		list.replaceAll(a -> {
			if (a != null) {
				return a;
			} else {
				return new Item();
			}
		});
	}

	private static void exampleCompute2(Map<String, List<Item>> map, Item x) {
		String dep = x.getDepartment();

		map.compute(dep, (f, list) -> {
			if (list == null)
				list = new ArrayList<>();
			list.add(x);
			return list;
		});
	}

	private static void exampleMerge(Map<String, Item> map, Item x) {
		String key = x.getDepartment();
		map.merge(key, x, (oldX, newX) -> {

			return oldX;

		});
	}
}
