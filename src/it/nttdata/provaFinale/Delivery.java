package it.nttdata.provaFinale;

import java.time.LocalDate;

public class Delivery {
	private String retailCode;
	private LocalDate deliveryDate;

	public Delivery() {

	}

	public Delivery(Row row) {
		this.retailCode = row.getRetail().getCode();
		this.deliveryDate = row.getDeliveryDate();
	}

	public String getRetailCode() {
		return retailCode;
	}

	public void setRetailCode(String retailCode) {
		this.retailCode = retailCode;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String toString() {
		return "delivery--> " + this.retailCode + ", "
				+ this.getDeliveryDate().toString();
	}
}
