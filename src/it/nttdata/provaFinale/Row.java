package it.nttdata.provaFinale;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Row {
	private Item item;
	private Retail retail;
	private LocalDate deliveryDate;

	public Row() {

	}

	public Row(String s) {
		String[] columns = s.split("\\s*\\t");
		retail = new Retail();
		retail.setCode(columns[0]);
		retail.setDescription(columns[1]);
		deliveryDate = LocalDate.parse(columns[2],
				DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		item = new Item();
		item.setCode(columns[3]);
		item.setDescription(columns[4]);
		item.setQuantity(Integer.parseInt(columns[5]));
	}

	public Delivery extractDelivery() {
		return new Delivery(this);
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Retail getRetail() {
		return retail;
	}

	public void setRetail(Retail retail) {
		this.retail = retail;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String toString() {
		return retail.getCode() + " " + retail.getDescription() + " "
				+ deliveryDate + " " + item.getCode() + " "
				+ item.getDescription() + " " + item.getQuantity();
	}
}
