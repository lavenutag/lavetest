package it.nttdata.provaFinale;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.YearMonth;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {
	public static void main(String[] args) {
		File file = new File("WS1.txt");
		try {
			List<Row> rows = parseFile(file.toPath());
			System.out.println(rows.get(0).toString());

			Stream<Row> stream = rows.stream();
			int total = total(stream, "ITEM9");
			System.out.println("total of ITEM9-->" + total);

			Consumer<String> report = s -> {
				System.out.println(s + "is present");
			};
			reportIfPresent(rows.stream(), "ITEM9", report);
			reportIfPresent(rows.stream(), "ITEM987", report);
			reportIfPresent(rows.stream(), "ITEMqua qua", report);

			MyRunnableClass r = new MyRunnableClass();
			reportIfAbsent(rows.stream(), "ITEM987", r);

			Map<YearMonth, List<Delivery>> delInMonth = deliveries(rows
					.stream());

			System.out.println(delInMonth.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method parses the file (reads it) and generates a list of Rows.
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static List<Row> parseFile(Path path) throws IOException {
		try (Stream<String> stream = Files.lines(path)) {
			return stream.map(Row::new).collect(Collectors.toList());
		}

	}

	/**
	 * Quanti pezzi di un articolo sono stati consegnati.
	 * 
	 * @param stream
	 * @param itemId
	 * @return int
	 */
	public static int total(Stream<Row> stream, String itemId) {
		Objects.requireNonNull(itemId);

		return stream.filter(row -> row.getItem().getCode().equals(itemId))
				.mapToInt((Row r) -> {
					return r.getItem().getQuantity();
				}).sum();
	}

	/**
	 * 
	 * @param stream
	 * @param itemId
	 * @param report
	 */
	public static void reportIfPresent(Stream<Row> stream, String itemId,
			Consumer<String> report) {
		if (stream.anyMatch((Row row) -> {
			return row.getItem().getCode().equals(itemId);
		})) {
			report.accept(itemId);
		}

	}

	/**
	 * 
	 * @param stream
	 * @return
	 */
	public static Map<YearMonth, List<Delivery>> deliveries(Stream<Row> stream) {
		return stream
				.map(Row::extractDelivery)
				.distinct()
				.collect(
						Collectors.groupingBy(delivery -> YearMonth
								.from(delivery.getDeliveryDate()),
								TreeMap::new, Collectors.toList()));
	}

	/**
	 * 
	 * 
	 */
	public static void reportIfAbsent(Stream<Row> stream, String itemId,
			Runnable report) {
		if (!stream.anyMatch((Row row) -> {
			return row.getItem().getCode().equals(itemId);
		})) {
			report.run();
		}
	}

	private static class MyRunnableClass implements Runnable {

		@Override
		public void run() {
			System.out.println("the item is not present");

		}

	}
}
