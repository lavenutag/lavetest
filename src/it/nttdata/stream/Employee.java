package it.nttdata.stream;

public class Employee {
	private Department dep;
	private double age;

	public Department getDepartment() {
		return this.dep;
	}

	public void setDepartment(Department d) {
		this.dep = d;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}
}
