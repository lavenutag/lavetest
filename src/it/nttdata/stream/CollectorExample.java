package it.nttdata.stream;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorExample {

	public static void example(Stream<Employee> source) {
		// *************************************************
		// Vediamo alcuni overloading del metodo groupingBy

		Map<Department, List<Employee>> map = source.collect(Collectors
				.groupingBy(Employee::getDepartment)); // è implicito il tipo di
														// down-stream lista

		map.forEach((dep, list) -> {
			// do something
		});

		// Raccogliamo gli Employee in una lista (specificando come down-stream
		// la lista)
		Map<Department, List<Employee>> map2 = source.collect(Collectors
				.groupingBy(Employee::getDepartment, Collectors.toList()));

		// Raccogliamo gli Employee in un set
		Map<Department, Set<Employee>> map3 = source.collect(Collectors
				.groupingBy(Employee::getDepartment, Collectors.toSet()));

		Map<Department, Double> something = source.collect(Collectors
				.groupingBy(Employee::getDepartment,
						Collectors.averagingDouble(Employee::getAge)));

	}

	public static String joining(Stream<String> source) {
		Collector<String, ?, String> coll = Collector.of(StringBuilder::new, (
				StringBuilder partial, String next) -> {
			partial.append(next);
		}, (StringBuilder p1, StringBuilder p2) -> {
			p1.append(p2);
			return p1;
		}, (StringBuilder acc) -> acc.toString());
		return source.collect(coll);
	}

	public static String joiningWithReduce(Stream<String> source) {
		return source.reduce(String::concat).orElse("");
	}
}
