package it.nttdata.stream;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public class SetExample {

	public static void main(String[] args) {
		Set<String> set1 = new HashSet<>();
		set1.add("element1");
		set1.add("element1b");
		set1.add("element2");
		Set<String> set2 = new HashSet<>();
		set2.add("element2");
		Set<String> set3 = new HashSet<>();
		set3.add("element2");
		Set<String> results = intersect(set1, set2, set3);
		System.out.println("elements in resuslt-->" + results.toString());

		Stream<Integer> source = Stream.of(2, 2, 4, 4, 8, 6, 10, 8, 10)
		// .parallel()
		;
		// Usiamo il reduce con l'identity, l'accumulator e il combiner:
		// l'identity definisce il risultato nel caso in cui lo stream sia
		// vuoto;
		// l'accumulator elabora due elementi per volta;
		// il combiner combina i risultati tornati via via dall'accumulator.
		// Questo viene però
		// chiamato soltanto se lo stream sorgente è parallelo. Se è
		// sequenziale, non viene chiamato.
		Long longSum = source.reduce(0L, (Long u, Integer t) -> {
			return u.longValue() + t.longValue();
		}, (Long l1, Long l2) -> {
			System.out.println("into combiner");
			return l1 + l2;
		});

		System.out.println("the sum is: " + longSum);

		// usiamo il parallelSort su una lista
		List<String> result = parallelSort(Stream.of("1", "5", "6", "2"));
		System.out.println("the parallelsorted list is: " + result);
	}

	private static class Reductor {
		private List<String> storage = new ArrayList<>();

		public List<String> getStorage() {
			return this.storage;
		}

		Reductor accumulate(String s) {
			// ..
			storage.add(s);
			return this;
		}

		Reductor combine(Reductor other) {
			// ...
			this.storage.addAll(other.getStorage());
			return this;
		}
	}

	public static List<String> toList(Stream<String> source) {
		// Reductor r = source.reduce(new Reductor(), (Reductor partial,
		// String next) -> partial.accumulate(next), (Reductor r1,
		// Reductor r2) -> r1.combine(r2));
		// return r.getStorage();

		Reductor r = source.reduce(new Reductor(), Reductor::accumulate,
				Reductor::combine);
		return r.getStorage();
	}

	synchronized public static List<String> parallelSort(Stream<String> source) {
		return source
				.parallel()
				.sorted()
				.reduce(new ArrayList<>(),
						(List<String> partial, String newString) -> {
							List<String> newPartial = partial.isEmpty() ? new ArrayList<>(
									partial) : partial;
							newPartial.add(newString);
							return newPartial;
						},
						(List<String> partialResultOfThread1,
						// the combiner is called when more
						// threads are working in parallel
								List<String> partialResultOfThread2) -> {
							partialResultOfThread1
									.addAll(partialResultOfThread2);
							return partialResultOfThread1;
						});
	}

	@SafeVarargs
	public static Set<String> intersect(Set<String>... sets) {
		// posso creare uno stream da un array di set
		return Stream.of(sets).parallel().reduce((s1, s2) -> {
			Set<String> copy = new HashSet<>(s1);
			copy.retainAll(s2);
			return copy;
		}).orElseThrow(IllegalArgumentException::new);
	}

	// Reduce con identity

	@SafeVarargs
	public static Set<String> intersect2(Set<String>... sets) {
		// posso creare uno stream da un array di set
		Set<String> myImplOfSet = null;
		Set<String> res = Stream.of(sets).parallel()
				.reduce(myImplOfSet, (s1, s2) -> {
					Set<String> copy = new HashSet<>(s1);
					copy.retainAll(s2);
					return copy;
				});
		return res;
	}

	@SafeVarargs
	public static Set<String> intersectExplicit(Set<String>... sets) {
		// posso creare uno stream da un array di set
		Stream<Set<String>> stream = Stream.of(sets);

		Optional<Set<String>> opt = stream.parallel().reduce(
				(Set<String> s1, Set<String> s2) -> {
					// per non modificare nessuno dei due set, usando il
					// retainAll,
					// creo una copia:
					Set<String> copy = new HashSet<>(s1);
					copy.retainAll(s2);
					return copy;
				});
		return opt.orElseThrow(IllegalArgumentException::new);
	}

	@SafeVarargs
	public static Set<String> intersectJava7(Set<String>... sets) {
		// Java doesn't allow to you, to do this
		// Set<String>[] array = new Set<String>[5];

		if (sets.length == 0)
			throw new IllegalArgumentException();
		Set<String> result = new HashSet<>(sets[0]);
		System.out.println("sets[0]-->" + result);
		for (int i = 1; i < sets.length; i++) {
			System.out.println("sets[i]-->" + sets[i]);
			result.retainAll(sets[i]);
		}

		return result;
	}
}
