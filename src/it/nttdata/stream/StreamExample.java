package it.nttdata.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

public class StreamExample {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("abc");
		list.add("Abc");
		list.add("aBc");
		list.add("abC");
		list.add("ABc");
		list.add("AbC");
		list.add("aBC");
		list.add("ABC");
		Comparator<String> com = (s1, s2) -> s1.compareToIgnoreCase(s2);
		for (int i = 0; i < 5; i++) {

			String maxList = list.stream().parallel().sorted(com)
			// .max(Comparator.naturalOrder())
					.max(com).get();
			System.out.println("list->" + maxList);

			HashSet<String> set = new HashSet<>(list);
			String maxSet = set.stream().parallel().sorted(com).max(com)
			// .max(Comparator.naturalOrder())
					.get();
			System.out.println("set ->" + maxSet);
		}
	}

	public static void test1(ArrayList<String> list) {
		HashSet<String> set = new HashSet<>(list);
		Comparator<String> com = (s1, s2) -> s1.compareToIgnoreCase(s2);
		System.out.println("@@@@@@@@@@LIST1");
		list.stream().sorted(com).forEach(s -> System.out.print(s + " "));
		System.out.println("@@@@@@@@@@LIST2");
		list.stream().parallel().sorted(com)
				.forEach(s -> System.out.print(s + " "));
		System.out.println("@@@@@@@@@@SET");
		set.stream().sorted(com).forEach(s -> System.out.print(s + " "));
		System.out.println("@@@@@@@@@@SET");
		set.stream().sorted(com).forEach(s -> System.out.print(s + " "));
		System.out.println("@@@@@@@@@@SET");
		set.stream().parallel().sorted(com)
				.forEach(s -> System.out.print(s + " "));
	}

}
