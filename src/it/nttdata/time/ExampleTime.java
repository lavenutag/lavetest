package it.nttdata.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.ValueRange;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Locale;

public class ExampleTime {
	public static void main(String[] args) {
		MonthDay feb29 = MonthDay.of(2, 29);
		LocalDate date = feb29.atYear(2015);

		System.out.println(date);
		// il 29 febbraio 2015 non esiste perciò la data restituita sarà
		// 2015-02-28
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, 4);
		System.out.println(cal.getTime());

		LocalDate now = LocalDate.now();
		LocalDate date2 = now.with(TemporalAdjusters.dayOfWeekInMonth(4,
				DayOfWeek.from(now)));
		System.out.println(date2);

		ZonedDateTime zdt = ZonedDateTime.now();
		for (FormatStyle fs : FormatStyle.values()) {
			DateTimeFormatter formatter = DateTimeFormatter
					.ofLocalizedDateTime(fs);
			String formatted = formatter.format(zdt);
			System.out.println(fs + "->" + formatted);
		}

		for (FormatStyle fs : FormatStyle.values()) {
			DateTimeFormatter formatter = DateTimeFormatter
					.ofLocalizedDateTime(fs).withLocale(Locale.ENGLISH);
			String formatted = formatter.format(zdt);
			System.out.println("formatted:---->" + fs + "->" + formatted);
			// ZonedDateTime parsed = formatter.parse(formatted,
			// ZonedDateTime::from); -->produce eccezione perchè perde
			// informazioni su minuti nel caso di
			// MEDIUM e SHORT
			TemporalAccessor parsed = formatter.parseBest(formatted,
					ZonedDateTime::from, LocalDateTime::from);

			System.out.println("parsed------>" + parsed);
		}

		DateTimeFormatter formatter = DateTimeFormatter
				.ofPattern("dd/MM/yyyy hh:mm:ss.SS");
		LocalDate dateToTry = LocalDate.of(2016, 1, 1);
		int weekBasedYear = dateToTry.get(WeekFields.ISO.weekBasedYear());
		System.out.println(weekBasedYear);
		ValueRange range = dateToTry
				.range(WeekFields.ISO.weekOfWeekBasedYear());
		System.out.println(range);
	}
}
