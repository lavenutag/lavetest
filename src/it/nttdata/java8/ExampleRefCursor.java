package it.nttdata.java8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class ExampleRefCursor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	private void example(Connection conn) throws SQLException {
		try (CallableStatement cs = conn.prepareCall("")) {
			cs.registerOutParameter(0, Types.REF_CURSOR);
			cs.executeQuery();

			try (ResultSet rs = cs.getObject("name", ResultSet.class)) {
				// ...
			}
		}
	}
}
