package it.nttdata.java8;

import java.util.Base64;

public class ExampleBase64 {

	public static void main(String[] args) {
		Base64.Encoder basicEncoder = Base64.getEncoder();
		String user = "";
		String pass = "p";
		String toEncode = user + ':' + pass + '\n' + "q";
		String encoded = basicEncoder.encodeToString(toEncode.getBytes());
		System.out.println(encoded);
	}

}
