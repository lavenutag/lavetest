package it.nttdata.string;

import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class StringOperators {

	private static String trimTrail(String s) {
		return s.replaceFirst("\\s*$", "");
	}

	public static void main(String[] args) {
		String toTrail = "ciao ";
		String trailed = trimTrail(toTrail);
		trailed = trimTrailOperator().apply(trailed);

		System.out.println(toTrail + trailed + "b");
		System.out.println(concatOperator().apply("string 1", "String 2"));
		System.out.println(prefixOperator("prefix").apply("conc"));
	}

	public static UnaryOperator<String> trimTrailOperator() {

		// Questo metodo si può anonimizzare
		// return new UnaryOperator<String>() {
		// @Override
		// public String apply(String t) {
		// return "0" +trimTrail(t);
		// }
		// };

		// Anonimzzazione 1
		// return (String t) ->{return "1" +trimTrail(t);
		// };

		// Anonimzzazione 2
		// return (String t) ->"2" +trimTrail(t);

		// Anonimizzazione 3
		// return (t) -> "3" + trimTrail(t);
		// questa lambda expression non
		// garantisce che l'operator
		// sia un singleton, tuttavia, essendo stateless (non ha variabili
		// interne), questo
		// non costituisce un problema.

		// Usando la sintassi equivalente che si serve di un method reference
		// otteniamo
		return StringOperators::trimTrail;
	}

	// Creiamo un altro operatore e definiamo l'implementazione del metodo apply
	// con
	// la sintassi del method reference
	public static UnaryOperator<String> prefixOperator(String prefix) {
		return prefix::concat;
	}

	public static BinaryOperator<String> concatOperator() {
		return String::concat;
	}
}
