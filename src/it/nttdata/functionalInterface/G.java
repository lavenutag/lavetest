package it.nttdata.functionalInterface;

public class G {
	public void execute(D d) {
		int result = d.d(7);
		System.out.println(result);
	}

	public static void main(String[] args) {
		E e = new E() {
			@Override
			public int e(long x) {
				return 8;
			}
		};
		G g = new G();
		// g.execute((D)e); //gives classCastException

		E lambdaE = (long x) -> {
			return (int) x;
		};
		D lambdaD = (long x) -> {
			return (int) x;
		};

	}
}
