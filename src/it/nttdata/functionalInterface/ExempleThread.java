package it.nttdata.functionalInterface;
/**
 * Un esempio di utilizzo delle lambda expression finalizzato
 * all'utilizzo del metodo run di una classe runnable che dovrebbe essere celato
 * ad un oggetto esterno alla classe.
 * @author Meteora
 *
 */
public class ExempleThread {

	public static void main(String[] args) {
		// In questo caso viene usata come classe che implementa l'interfaccia Runnamble
		// una classe diversa da quella corrente
		ExampleRunnable runnable = new ExampleRunnable();
		Thread th = new Thread(runnable);
		th.start();
		
		// In questo caso viene usata una inner class
		Thread th2 = new Thread(new InnerRunnable());
		th2.start();
		
		// In questo caso viene usata una anonymus class
				Thread th3 = new Thread(new Runnable(){

					@Override
					public void run() {
						somethingToDo("th3");
						
					}
					
				});
				th3.start();
				
		
		//usando la lambda expression, non indico il nome del metodo..lo invoco direttamente
		// con le parentesi affiancate dall'operatore lambda -> e dal corpo del metodo tra {..}
		Thread th4 = new Thread(()->{
			somethingToDo("th4");
		});
		th4.start();

	}
	private static void somethingToDo(String thname) {
		System.out.println("Hello from anonymous class " + thname);
	}
	private static class InnerRunnable implements Runnable{

		@Override
		public void run() {
			System.out.println("Hello World from inner class!!!!");
		}
	}
	
	
}
