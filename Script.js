print('ciao');

function hello(name){
	print("hello "+name+" tutto bene?");
}

function run(){
	print("this is run!");
}

var obj = new Object(){
	run : function(){
		print("ciao dall'oggetto obj e dal suo run!");
	}
}

function testJavaObj(){
	jo.run();
}
var Runnable = Java.type('java.lang.Runnable');
var jsObj = new Runnable(){
	run: function(){
		print('Hello from jsObj');
	}
}

function testCambioRun(){
	obj.run = function(){
		print('obj run overridden');
	};
	
	jsObj.run = function(){
		print('jsObj run overridden');
	};
	obj.run();
	jsObj.run();
}

